####################
# setup networking #
####################
wpa_passphrase '$SSID $PASSWORD' | tee /etc/wpa_supplicant/wpa_supplicant.conf
wpa_supplicant -B -c /etc/wpa_supplicant/wpa_supplicant.conf -i wlan0

systemctl restart dhcpcd
ping archlinux.org

##############
# setup disk #
##############
# setup disk with the following partition scheme:
# * single physical parition marked as boot
# * / with size 50000M (50 GBs)
# * /tmp with size 1000M (1 GB)
# * /var with size 4000M (4 GBs)
# * swap with size 2000M (4 GBs)
# * /home with the rest of disk

# delete all data on disk
dd if=/dev/zero of=/dev/sda  bs=512  count=1

# create paritions
fdisk /dev/sda:

n
p

ENTER
ENTER

ENTER
a

n
p

w

# encrypt the linux filesystem
cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 5000 --use-random luksFormat /dev/sda2

# open the linux filesystem
cryptsetup luksOpen /dev/sda1 arch

# create lvm group on arch
vgcreate archlvm /dev/mapper/arch

# create lvm partitions
lvcreate --name root -L 50G archlvm
lvcreate --name tmp -L 1G archlvm

lvcreate --name var -L 4G archlvm
lvcreate --name swap -L 2G archlvm

lvcreate --name home -l 100%FREE archlvm

# create filesystems
mkfs.ext4 -L root /dev/archlvm/root
mkfs.ext4 -L tmp /dev/archlvm/tmp

mkfs.ext4 -L var /dev/archlvm/var
mkfs.ext4 -L home /dev/archlvm/home

mkswap /dev/archlvm/swap

# mount filesystems
mount /dev/archlvm/root /mnt

mkdir /mnt/tmp
mount /dev/archlvm/tmp /mnt/tmp

mkdir /mnt/var
mount /dev/archlvm/var /mnt/var

mkdir /mnt/home
mount /dev/archlvm/home /mnt/home

##################
# install system #
##################
pacstrap -i /mnt base base-devel

# chroot into the system
arch-chroot /mnt

# set ownership and permissions of /
chown root:root /
chmod 755 /

# set root password
passwd root

# set hostname
echo "host" > /etc/hostname

###############
# setup fstab #
###############
# edit /etc/fstab and add the following

# <file system>    <dir>     <type>    <options>                 <dump>    <pass>
/dev/archlvm/root  /         ext4      defaults                  0         1
/dev/archlvm/tmp   /tmp      ext4      nodev,nosuid,noexec       0         0
/dev/archlvm/var   /var      ext4      nodev,nosuid,noexec       0         1
/dev/archlvm/home  /home     ext4      nodev,nosuid              0         2
/dev/archlvm/swap  none      swap      nodev,noexec,nosuid       0         0

#########################
# setup locale and time #
#########################
# uncomment en_US locale and generate locale
sed -i 's/#en_US.UTF-8 UTF/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf
export LANG=en_US.UTF-8

# set system time to UTC and set the hardware clock to UTC
ln -s /usr/share/zoneinfo/UTC /etc/localtime
hwclock --systohc --utc

############################
# setup grub and initramfs #
############################
# install grub, apparmor, ntp, dhcpcd, lvm2, linux and linux-hardened
pacman -S grub-bios apparmor ntp dhcpcd linux linux-hardened

# add encrypt and lvm2 to hooks in /etc/mkinitcpio.conf
HOOKS(base udev autodetect modconf block encrypt lvm2 filesystems keyboard fsck)

# get UUID of /dev/sda2
blkid | grep "sda2"

# add support for decrypting at boot and for apparmor by editing /etc/default/grub
GRUB_ENABLE_CRYPTODISK=y
GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor audit=1 cryptdevice=DEVICE-UUID:arch"

# add 'encrypt' to HOOKS in /etc/mkinitcpio.conf and make initramfs
mkinitcpio -p linux
mkinitcpio -p linux-hardened

# install grub and save config
grub-install --recheck /dev/sda
grub-mkconfig --output /boot/grub/grub.cfg

################################
# enable services and clean up #
################################
systemctl enable apparmor
systemctl enable auditd

systemctl enable ntpd
systemctl enable dhcpcd

# exit chroot and save wpa_supplicant.conf
^D
cp /etc/wpa_supplicant/wpa_supplicant.conf /mnt/wpa_supplicant/wpa_supplicant.conf

reboot

##################
# update systemn #
##################
# login as root and then update the system
pacman -Syu

#########################
# setup password policy #
#########################
# add the following to /etc/pam.d/passwd
# set password policy to:
# * require a minimun of 10 characters, 1 digit, 1 uppercase, 1 other character and 1 lowercase for passwords
# * prompt the user 3 times for a password incase of a mistype and suspend user login 10 minuets after 5 failed attempts to login
password required pam_cracklib.so retry=3 minlen=10 difok=6 dcredit=-1 ocredit=-1 lcredit=-1
password required pam_unix.so use_authtok sha512 shadow

auth optional pam_faildelay.so delay=4000000
auth required pam_tally2.so deny=5 unlock_time=600 inerr=succeed file=/var/log/tallylog

##################
# setup firejail #
##################
# install firejail
pacman -S firejail

# add apparmor integration
apparmor_parser -r /etc/apparmor.d/firejail-default

# use firejail for all applications for which it has profiles for
firecfg

#######################
# setup rootless xorg #
#######################
# install xorg-minimal
pacman -S xorg-minimal

# set Xorg to not use root when run from startx
echo "needs_root_rights=no" >> /etc/X11/Xwrapper.config

############################
# setup battery management #
############################
# suspend the system when the battery level drops to 15% or lower
echo SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", ATTR{capacity}=="[0-15]", RUN+="/usr/bin/systemctl hibernate"

# lock screen on suspend by creating /etc/systemd/system/lock.service
[Unit]
Description=Lock the screen on resume from suspend

[Service]
Enviroment=DISPLAY=:0
ExecStart=$LOCKSCREEN

[Install]
WantedBy=suspend.target

# enable lock.service
systemctl enable lock.service

###############
# setup umask #
###############
echo "umask 0077" /etc/profile

##############
# setup sudo #
##############
# install sudo
pacman -S sudo

# make the following edits to /etc/sudoers
Defaults use_pty
Defaults log_host, log_year, logfile="/var/log/sudo.log"

Defaults log_input, log_output
Defaults lecutre="never"

Defaults passwd_tries=3
Defaults passwd_timeout=2

Defaults insults

%wheel ALL=(ALL:ALL) ALL

######################
# setup user account #
######################
# create user account and add them to wheel
useradd -m -G wheel user

# set user password
passwd user

# log out and log back in as user then lock the root account
sudo passwd -l root
